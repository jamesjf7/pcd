﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }


        bool loaded;
        private void Form1_Load(object sender, EventArgs e)
        {
            loaded = false;
            comboBox1.Items.AddRange(DIP.ListName);
        }

        private void btnImg_Click(object sender, EventArgs e)
        {
            var result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                var path  = ofd.FileName;
                Image img = Image.FromFile(path);
                pictureBox1.Image = img;
                loaded = true;
            }
        }

        private void btnProc_Click(object sender, EventArgs e)
        {
            if (!loaded){
                MessageBox.Show("No Image Loaded!!!");
                return;
            }
            else
            {
                Image img   = pictureBox1.Image;
                int procIdx = comboBox1.SelectedIndex;
                if (comboBox1.SelectedIndex == 0) {
                    Image[] paramImg = DIP.ProcessBitplane(img, procIdx);
                    new FormResult(paramImg).ShowDialog();
                }
                else if (comboBox1.SelectedIndex == 3 || comboBox1.SelectedIndex == 4) {
                    int size = (int)numericUpDown1.Value;
                    Image paramImg = DIP.Process(img, procIdx, size);
                }
                else {
                    Image paramImg = DIP.Process(img, procIdx, 0);
                }
                MessageBox.Show("Done");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label2.Text = "";
            numericUpDown1.Enabled = false;
            if (comboBox1.SelectedIndex == 3 || comboBox1.SelectedIndex == 4){
                label2.Text = "Size";
                numericUpDown1.Enabled = true;
                numericUpDown1.Maximum = 5;
                numericUpDown1.Minimum = 0;
            }
        }
    }
}
