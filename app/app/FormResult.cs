﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app
{
    public partial class FormResult : Form
    {
        Bitmap[] bmp = new Bitmap[9];
        public FormResult(Image[] img)
        {
            InitializeComponent();
            for (int i = 0; i < 9; i++)
            {
                this.bmp[i] = new Bitmap(img[i]);
            }
        }

        private void FormResult_Load(object sender, EventArgs e)
        {

            //pictureBox1.Size = new Size(
            //    bmp.Width, bmp.Height
            //);
            pictureBox1.Image = bmp[0];
            pictureBox2.Image = bmp[1];
            pictureBox3.Image = bmp[2];
            pictureBox4.Image = bmp[3];
            pictureBox5.Image = bmp[4];
            pictureBox6.Image = bmp[5];
            pictureBox7.Image = bmp[6];
            pictureBox8.Image = bmp[7];
            pictureBox9.Image = bmp[8];

            //pictureBox1.Location = new Point(
            //    this.Size.Width /2 - bmp.Width /2,
            //    this.Size.Height/2 - bmp.Height/2
            //);
        }
    }
}
