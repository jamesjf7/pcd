﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app{
    public class DIP {
        public static string[] ListName {
            get => new string[]{
                "BitPlane Slicing",
                "Low Pass Filtering",
                "High Pass Filtering",
                "Median Filtering",
                "Intensity Slicing",
                "Sobel",
                "Huffman Encoding",
                "Chain Code"
            };
        }

        public static Bitmap[] ProcessBitplane(Image img, int idx)
        {
            Dictionary<int, Func<Image, Bitmap[]>> dict = new Dictionary<int, Func<Image, Bitmap[]>>();
            dict[0] = bitplaneSlicing;
            return dict[idx](img);
        }
        public static Bitmap Process(Image img, int idx, int size) {
            // var 2 jelek mau diganti
            Dictionary<int, Func<Image, int, Bitmap>> dict = new Dictionary<int, Func<Image, int, Bitmap>>();
            dict[1] = LPF;
            dict[2] = HPF;
            dict[3] = MedianFilter;
            dict[4] = IntensitySlicing;
            dict[5] = Sobel;
            dict[6] = Huffman;
            dict[7] = ChainCode;
            System.Windows.Forms.MessageBox.Show("Done");
            return dict[idx](img, size);
        }

        public static int GrayscaleFromRGB(Color color)
            => GrayscaleFromRGB(color.R, color.G, color.B);

        public static int GrayscaleFromRGB(int r, int g, int b)
            => (int)(r * 0.07 + g * 0.72 + b * 0.21);

        public static Color ColorFromGrayscale(int gray)
            => Color.FromArgb(gray, gray, gray);

        public static Bitmap HPF(Image img, int size = 0) {
            int[,] hpf ={
                {-1,-1,-1},
                {-1, 8,-1},
                {-1,-1,-1}
            };

            Bitmap bmp = new Bitmap(img);
            Bitmap dbmp1 = new Bitmap(bmp.Width, bmp.Height);
            int[,] color = new int[bmp.Height, bmp.Width];
            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    var warna = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(warna);
                    color[i, j] = gray;
                }
            }

            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    int accH = 0;
                    for (int k = -1; k < 2; k++)
                        for (int l = -1; l < 2; l++) {
                            int
                                x = j + l,
                                y = i + k;
                            if (x < 0) x = 0;
                            if (x >= bmp.Width) {
                                x = bmp.Width - 1;
                            }
                            if (y < 0) y = 0;
                            if (y >= bmp.Height) {
                                y = bmp.Height - 1;
                            }
                            accH += color[y, x] * hpf[k + 1, l + 1];
                        }
                    int hasil_high;
                    hasil_high = (int)Math.Round(accH * 1.0 / 9);
                    hasil_high = Math.Max(hasil_high, 0);
                    hasil_high = 255 - hasil_high;
                    dbmp1.SetPixel(j, i, Color.FromArgb(hasil_high, hasil_high, hasil_high));
                }
            }
            dbmp1.Save("../../Resources/hpf.jpeg");
            return dbmp1;
        }
        public static Bitmap LPF(Image img, int size = 0) {
            int[,] lpf = {
                {1,1,1},
                {1,1,1},
                {1,1,1}
            };

            Bitmap bmp = new Bitmap(img);
            Bitmap dbmp1 = new Bitmap(bmp.Width, bmp.Height);
            int[,] color = new int[bmp.Height, bmp.Width];
            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    var warna = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(warna);
                    color[i, j] = gray;
                }
            }

            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    int accL, accH = accL = 0;
                    for (int k = -1; k < 2; k++)
                        for (int l = -1; l < 2; l++) {
                            int
                                x = j + l,
                                y = i + k;
                            if (x < 0) {
                                x = 0;
                            }
                            if (x >= bmp.Width) {
                                x = bmp.Width - 1;
                            }
                            if (y < 0) {
                                y = 0;
                            }
                            if (y >= bmp.Height) {
                                y = bmp.Height - 1;
                            }
                            accL += color[y, x] * lpf[k + 1, l + 1];
                        }
                    int hasil_low;
                    hasil_low = (int)Math.Round(accL * 1.0 / 9);
                    dbmp1.SetPixel(j, i, Color.FromArgb(hasil_low, hasil_low, hasil_low));
                }
            }
            dbmp1.Save("../../Resources/lpf.jpeg");
            return bmp;
        }
        public static Bitmap MedianFilter(Image img, int size) {
            if (size % 2 == 1) {             
                Bitmap bmp = new Bitmap(img);
                int[,] color = new int[bmp.Height, bmp.Width];
                for (int i = 0; i < bmp.Height; i++){
                    for (int j = 0; j < bmp.Width; j++){
                        var warna = bmp.GetPixel(j, i);
                        int gray = GrayscaleFromRGB(warna);
                        color[i, j] = gray;
                    }
                }
                Bitmap dbmp1 = new Bitmap(bmp.Width, bmp.Height);
                for (int i = 0; i < bmp.Height; i++){
                    for (int j = 0; j < bmp.Width; j++){
                        List<int> test = new List<int>();
                        for (int k = 0 - size / 2; k < 0 + size / 2; k++)
                            for (int l = 0 - size / 2; l < 0 + size / 2; l++){
                                int
                                    x = j + l,
                                    y = i + k;
                                if (x < 0)
                                    x = 0;
                                if (x >= bmp.Width)
                                    x = bmp.Width - 1;
                                if (y < 0)
                                    y = 0;
                                if (y >= bmp.Height)
                                    y = bmp.Height - 1;
                                test.Add(color[y, x]);
                            }
                        test.Sort();
                        int jumlahdata = test.Count();
                        int hasilmedian = test[jumlahdata / 2];
                        dbmp1.SetPixel(j, i, Color.FromArgb(hasilmedian, hasilmedian, hasilmedian));
                    }
                }
                dbmp1.Save("../../Resources/medianfilter.jpeg");
                return dbmp1;
            }
            else {
                MessageBox.Show("Size harus ganjil");
                return new Bitmap(img);
            }
        }
        public static Bitmap[] bitplaneSlicing(Image img) {
            Bitmap bmp = new Bitmap(img);
            Bitmap allBmp = new Bitmap(bmp.Width * 3, bmp.Width * 3);
            Bitmap[] listbmp = new Bitmap[9];
            for (int i = 0; i < 9; i++)
                listbmp[i] = new Bitmap(img);
            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    var col = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(col);
                    bmp.SetPixel(j, i, Color.FromArgb(gray, gray, gray));
                    listbmp[8].SetPixel(j, i, Color.FromArgb(gray, gray, gray));
                    for (int k = 0; k < 8; k++) {
                        listbmp[k].SetPixel(j, i,
                            (gray % 2 == 1) ? Color.White : Color.Black
                        );
                        gray >>= 1;
                    }
                }
            }
            return listbmp;
        }
        public static Bitmap IntensitySlicing(Image img, int size) {
            Color[] c = new Color[5] {
                Color.Red,
                Color.Yellow,
                Color.Blue,
                Color.Green,
                Color.Purple
            };
            Bitmap bmp = new Bitmap(img);
            int max_value = int.MinValue, min_value = int.MaxValue;
            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    var col = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(col);
                    max_value = Math.Max(max_value, gray);
                    min_value = Math.Min(min_value, gray);
                }
            }
            int divider = (max_value - min_value) / (size - 1);
            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    var col = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(col);
                    bmp.SetPixel(j, i, c[gray / divider]);
                }
            }
            bmp.Save("../../Resources/intensity_slicing_result.jpeg");
            return bmp;
        }
        public static Bitmap Sobel(Image img, int no_use = 0) {
            int[,] gx = {
                {-1, 0, 1},
                {-2, 0, 2},
                {-1, 0, 1}
            };
            int[,] gy = {
                { 1, 2, 1},
                { 0, 0, 0},
                {-1,-2,-1}
            };
            Bitmap bmp = new Bitmap(img);
            Bitmap bmp_horizontal = new Bitmap(img);
            Bitmap bmp_vertical = new Bitmap(img);
            int[,] color = new int[bmp.Height, bmp.Width];
            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    var warna = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(warna);
                    color[i, j] = gray;
                }
            }
            int pixel_value_x, pixel_value_y;
            int gradient, pixel_value = 0;
            for (int i = 1; i < bmp.Height - 1; i++) {
                for (int j = 1; j < bmp.Width - 1; j++) {
                    pixel_value_x = pixel_value_y = 0;
                    for (int k = -1; k < 2; k++)
                        for (int l = -1; l < 2; l++) {
                            pixel_value_x += gx[k + 1, l + 1] * color[i + k, j + l];
                            pixel_value_y += gy[k + 1, l + 1] * color[i + k, j + l];
                        }
                    pixel_value = Math.Abs(pixel_value_x) + Math.Abs(pixel_value_y);
                    pixel_value = pixel_value > 255 ? 255 : pixel_value;
                    pixel_value = pixel_value < 0 ? 0 : pixel_value;
                    bmp.SetPixel(j, i, Color.FromArgb(pixel_value, pixel_value, pixel_value));
                    pixel_value_x = Math.Abs(pixel_value_x);
                    pixel_value_y = Math.Abs(pixel_value_y);
                    pixel_value_x = pixel_value_x > 255 ? 255 : pixel_value_x;
                    pixel_value_x = pixel_value_x < 0 ? 0 : pixel_value_x;
                    pixel_value_y = pixel_value_y > 255 ? 255 : pixel_value_y;
                    pixel_value_y = pixel_value_y < 0 ? 0 : pixel_value_y;
                    bmp_horizontal.SetPixel(j, i, Color.FromArgb(pixel_value_x, pixel_value_x, pixel_value_x));
                    bmp_vertical.SetPixel(j, i, Color.FromArgb(pixel_value_y, pixel_value_y, pixel_value_y));
                }
            }
            bmp.Save("../../Resources/sobel_result.jpeg");
            bmp_horizontal.Save("../../Resources/sobel_result_horizontal.jpeg");
            bmp_vertical.Save("../../Resources/sobel_result_vertical.jpeg");
            return bmp;
        }


        public static Bitmap resChainCode;
        public static string chainCodeNumber;
        public static Bitmap ChainCode(Image img, int size = 0) {
            Bitmap bmp = new Bitmap(img);
            int[,] color = new int[bmp.Width, bmp.Height];
            int x = int.MaxValue, y = int.MaxValue; 
            for (int i = 0; i < bmp.Height; i++) {
                for (int j = 0; j < bmp.Width; j++) {
                    var warna = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(warna) > 100 ? 255 : 0;
                    color[j, i] = gray;
                    if (x > j && y > i && color[j, i] == 0) {
                        x = j;
                        y = i;
                    }
                }
            }
            chainCodeNumber = "";
            resChainCode = new Bitmap(bmp.Width, bmp.Height);
            for (int i = 0; i < resChainCode.Height; i++){
                for (int j = 0; j < resChainCode.Width; j++){
                    resChainCode.SetPixel(j, i, Color.White);
                }
            }
            bool success = SolveChainCode(color,x,y,x,y,0, bmp.Width, bmp.Height);
            if (success){
                File.WriteAllText("../../Resources/chaincoderesult.txt", chainCodeNumber);
                resChainCode.Save("../../Resources/chain_code_result.jpeg");
                return resChainCode;
            }
            else {
                MessageBox.Show("This image cannot be chain code");
                return bmp;
            }
        }
        public static bool SolveChainCode(int[,] color,int px, int py, int targetX, int targetY, int directionIdx, int width, int height) {
            int[] directionX = new int[] { 1,  1,  0, -1, -1, -1, 0, 1 };
            int[] directionY = new int[] { 0, -1, -1, -1,  0,  1, 1, 1 };
            bool success = false;
            int nextPx,
                nextPy;
            if (px == targetX && py == targetY && chainCodeNumber != "") 
                return true;
            do{
                nextPx = px + directionX[directionIdx];
                nextPy = py + directionY[directionIdx];
                
                if (nextPx < width && nextPy < height && nextPx >= 0 && nextPy >= 0 && color[nextPx, nextPy] == 0 
                && GrayscaleFromRGB(resChainCode.GetPixel(nextPx,nextPy)) == 255){ 
                    resChainCode.SetPixel(nextPx, nextPy, Color.Black);
                    chainCodeNumber += directionIdx+"";
                    success = SolveChainCode(color, nextPx, nextPy, targetX, targetY, 0, width, height);
                    if (!success){
                        resChainCode.SetPixel(nextPx, nextPy, Color.White);
                        chainCodeNumber.Remove(chainCodeNumber.Length - 1);
                    }
                }
                directionIdx++;
            } while ((nextPx != targetX || nextPy != targetY) && directionIdx < directionX.Length && !success);
            return success;
        }

        public static Bitmap Huffman(Image img, int size = 0) {
            Bitmap bmp = new Bitmap(img);
            Bitmap res = new Bitmap(bmp.Width, bmp.Height);
            
            int[,] color = new int[bmp.Width, bmp.Height];
            for (int i = 0; i < bmp.Height; i++){
                for (int j = 0; j < bmp.Width; j++){
                    var warna = bmp.GetPixel(j, i);
                    int gray = GrayscaleFromRGB(warna);
                    color[j, i] = gray;
                }
            }
            HuffmanTree ht = new HuffmanTree();
            ht.BuildHuffmanTree(color, bmp.Height, bmp.Width);
            Dictionary<int, int> listEncodedPixel = ht.Encode(color, bmp.Height, bmp.Width);
            Console.WriteLine("<Value> - <Frequency> - <Result>");
            File.WriteAllText("../../Resources/HuffmanResult.txt", "<Value> - <Frequency> - <Result>\n");
            ht.preorder(ht.Root);
            for (int i = 0; i < bmp.Height; i++)
                for (int j = 0; j < bmp.Width; j++)
                    res.SetPixel(j, i, ColorFromGrayscale(listEncodedPixel[color[j, i]]));
            res.Save("../../Resources/huffman.jpg");
            return res;
        }
        
    }
    public class Node {
        public int Value { get; set; } //
        public int Frequency { get; set; }
        public Node Right { get; set; }
        public Node Left { get; set; }
        public List<bool> Traverse(int value, List<bool> data){
            if (Right == null && Left == null){
                if (value == this.Value)
                    return data;
                else
                    return null;
            }
            else{
                List<bool> left = null;
                List<bool> right = null;
                if (Left != null){
                    List<bool> leftPathData = new List<bool>();
                    leftPathData.AddRange(data);
                    leftPathData.Add(false);
                    left = Left.Traverse(value, leftPathData);
                }
                if (Right != null){
                    List<bool> rightPathData = new List<bool>();
                    rightPathData.AddRange(data);
                    rightPathData.Add(true);
                    right = Right.Traverse(value, rightPathData);
                }
                if (left != null)
                    return left;
                else
                    return right;
            }
        }
    }
    public class HuffmanTree {
        private List<Node> nodes = new List<Node>();
        public Node Root { get; set; }
        public Dictionary<int, int> data = new Dictionary<int, int>();
        public void preorder(Node root, string value = ""){
            if (root != null) {
                preorder(root.Left, value + "0");
                preorder(root.Right, value + "1");
                if (IsLeaf(root)) {
                    Console.WriteLine((root.Value + "").PadRight(6) + "  - " + (root.Frequency + "").PadRight(10) + "  - " + value);
                    File.AppendAllText("../../Resources/HuffmanResult.txt", (root.Value + "").PadRight(6) + "  - " + (root.Frequency + "").PadRight(10) + "  - " + value + "\n");
                }
            }
        }
        public void BuildHuffmanTree(int[,] source, int height, int width){
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++){
                    if (!data.ContainsKey(source[j, i]))
                        data.Add(source[j, i], 0);
                    data[source[j, i]]++;
                }
            foreach (KeyValuePair<int, int> i in data){
                nodes.Add(new Node() { Value = i.Key, Frequency = i.Value });
            }
            while (nodes.Count >= 2){
                List<Node> orderedNodes = nodes.OrderBy(node => node.Frequency).ToList<Node>();
                if (orderedNodes.Count >= 2){
                    List<Node> taken = orderedNodes.Take(2).ToList<Node>();
                    Node parent = new Node(){
                        Value = -1,
                        Frequency = taken[0].Frequency + taken[1].Frequency,
                        Left = taken[1],
                        Right = taken[0]
                    };
                    nodes.Remove(taken[0]); 
                    nodes.Remove(taken[1]); 
                    nodes.Add(parent); 
                }
                this.Root = nodes.FirstOrDefault();
            }
        }

        public Dictionary<int, int> Encode(int[,] source, int height, int width){
            List<bool> encodedSource = new List<bool>();
            Dictionary<int, int> decimalEncodedResult = new Dictionary<int, int>();
            for (int i = 0; i < height; i++){
                for (int j = 0; j < width; j++){
                    if (!decimalEncodedResult.ContainsKey(source[j, i])){
                        List<bool> encodedSymbol = this.Root.Traverse(source[j, i], new List<bool>());
                        int dec = 0;
                        string binary = "";
                        for (int k = 0; k < encodedSymbol.Count; k++){
                            if (encodedSymbol[k])
                                dec += (int)Math.Pow(2, encodedSymbol.Count - (k + 1));
                            binary += Convert.ToInt32(encodedSymbol[k]) + "";
                        }
                        decimalEncodedResult.Add(source[j, i], dec);
                    }
                }
            }
            decimalEncodedResult.OrderByDescending(value => value);
            return decimalEncodedResult;
        }
        public bool IsLeaf(Node node) => (node.Left == null && node.Right == null);
    }
}
